# HMRC Demo Page


### Getting Started

Checkout this repo, install dependencies, then start `http-server` process with the following:

```
> git clone eser@bitbucket.org:eser/hmrc_demo_page.git
> cd hmrc_demo_page
> npm install
> http-server -p 8000
```

then open browser and go [http://localhost:8000](http://localhost:8000)

